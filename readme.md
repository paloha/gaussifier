# Not supported anymore
This package is not supported anymore. It was replaced by Redistributor: 
https://gitlab.com/paloha/redistributor


# Gaussifier
An algorithm for automatic transformation of data from arbitrary
distribution into Gaussian distribution. Distribution of data is
learned from the data itself. Transformation is piecewise
linear, monotonic and invertible. The transformation preserves
relative distances between data points in linearly transformed pieces.

Implemented as a Scikit-learn transformer.
Can be fitted on training data and saved to be used later for
transforming the validation or testing data.

More detailed description can be found in the docstring of the Gaussifier()
class which is located in the file ```gaussifier/gaussifier.py```.

![gaussifier](/uploads/4ea21ab065217f726415094e7da7f59f/gaussifier.png)

## Installation
```
git clone git@gitlab.com:paloha/gaussifier.git
cd gaussifier
python3 -m virtualenv .venv
source .venv/bin/activate
pip install .
```

## Example of usage
Simplest possible example:
```
from gaussifier import Gaussifier
g = Gaussifier()
g.fit(training_data)
transformed = g.transform(validation_data)
inversed = g.inverse_transform(transformed)
```
For more detailed examples with plots, take a look into the iPython notebook.

## Limitations
* Data must be from interval <0.,1.>
* Does not work when there are data points with the exact
  same value, therefore data should not be sparse - multiple zeros etc.

# License
This project is licensed under the terms of the MIT license.
See license.txt for details.

# Acknowledgement
This work was supported by International Mobility of Researchers (CZ.02.2.69/0.0/0.0/16027/0008371). 
![opvvv](/uploads/f175373ba17576104586b4d57f962fdb/opvvv.jpg)
