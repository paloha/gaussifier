from setuptools import setup

setup(
    name='gaussifier',
    version='0.1',
    author='Pavol Harar',
    author_email='pavol.harar@gmail.com',
    packages=['gaussifier'],
    description='Package for learning the data distribution from the data itself and transforming it to another distribution.',
    long_description=open('readme.md').read(),
    install_requires=['numpy>=1.11.0', 'scipy>=0.19.1', 'scikit_learn>=0.19.1'],
    license='MIT',
)
