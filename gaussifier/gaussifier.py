# -*- coding: utf-8 -*-
from __future__ import division

import operator
import numpy as np
import scipy.integrate as integrate
from scipy.stats import norm
from sklearn.base import TransformerMixin


class Gaussifier(TransformerMixin):
    """
    An algorithm for automatic transformation of data from arbitrary
    distribution into Gaussian distribution.  Distribution of data is
    learned from the data itself. Transformation is piecewise
    linear, monotonic and invertible. The transformation preserves
    relative distances between data points in linearly transformed pieces.

    Implemented as a Scikit-learn transformer.
    Can be fitted on training data and saved to be used later for
    transforming the validation or testing data.

    Time complexity
    --------
    This algorithm can scale to a large number of samples > 1e08.
    There are three main aspects of performance of this class.
    1. Time complexity of the fit()
    2. Time complexity of the transform()
    3. Value of the validate_input flag

    Fitting primarily depends on the fact whether the data are sorted or not.
    Sorting a big array takes 99% of the total fitting time which can be avoided
    by providing sorted data to fit().

    Transformation is implemented to work on non sorted data, because this is
    going to be the case most of the time in the real world scenario, even
    though sorted version could be much faster. Nbins plays a big role in the
    total duration of transform().

    Ideal scenario is to fit on sorted data and use nbins < 51.
    Nbins = 51 is good enough to get a very gaussian looking histogram.

    To further speed up the process, you can set self.validate_input = False.
    This will turn of most of the sanity checks on the input data. Do this
    only when you know that the data meet all the requirements.

    On Intel® Core™ i7-4700MQ CPU @ 2.40GHz (self.validate_input = False):
    n: 1e04, bins: 51,  time: fit(sorted)     107.0747ms | transform 2.5651ms
    n: 1e04, bins: 51,  time: fit(unsorted)   111.2702ms | transform 2.1045ms
    n: 1e04, bins: 333, time: fit(sorted)     664.3775ms | transform 7.0691ms
    n: 1e04, bins: 333, time: fit(unsorted)   658.4051ms | transform 7.5712ms

    n: 1e08, bins: 51,  time: fit(sorted)     221.6120ms | transform 15736.4178ms
    n: 1e08, bins: 51,  time: fit(unsorted) 10938.6756ms | transform 16711.9286ms
    n: 1e08, bins: 333, time: fit(sorted)     772.2814ms | transform 84326.6544ms
    n: 1e08, bins: 333, time: fit(unsorted) 11606.6661ms | transform 81854.3193ms

    Attributes
    --------
    self.mother : scipy probability density function
        Gaussian mother function with integral on <0,1> = 1

    validate_input: boolean, optional, default True
        Flag that allows validation on the input data if True.
        It is good to leave it set to True, but set it to False, if the performance
        is crucial and you are sure, that your data satisfy the requirements.

    self.fitted : boolean, default False
        Flag that is toggled on fit()

    self.N : int, default None
        Total number of data samples used in fit()

    self.nbins : int, default 0
        Number of bins of histogram to optimize
        Must be > 2 and odd.
        Automatic limit = 333.

    self.bi : float, defult None
        Bin Interval - Width of one ideal bin.
        Equal to 1 / self.nbins.

    self.dba : array of ints, default None
        Desired Bin Amounts - amount of data points for each
        bin, as close to gaussian distribution as possible.
        Totalling to number of data samples. Depends on
        self.N and self.nbins.

    self.dbf : array of floats, detault None
        Desired Bin Frontiers - positions on x axis dividing
        the x axis into n equally spaced bins specified by self.nbins.

    self.rbf : array of floats, detault None
        Real Bin Frontiers - positions on x axis separating
        the bins shifted so the amounts of data points in each bin are
        according to self.dba.

    self.ratios : array of floats, default None
        Ratios of desired bin width to real bin width
        Piecewise linear transformations are computed based on
        these ratios and start position of each desired and real bin.

    Limitations
    --------
    - Data must be from interval <0.,1.>
    - Does not work when there are data points with the exact
      same value, therefore data should not be sparse - multiple zeros etc.

    Example
    --------
    # Generating random unsorted training data
    >>> x = np.random.rand(500)  # Uniform

    # Fitting the Gaussifier
    >>> g = gaussifier.Gaussifier()
    >>> g.fit(x, nbins=0, maxnbins=101, sorted=False)

    # Saving and loading the fitted Gaussifier
    >>> joblib.dump(g, 'gaussifier.pkl')
    >>> g = joblib.load('gaussifier.pkl')

    # Transform and inverse transform the training data
    >>> transformed = g.transform(x)
    >>> inversed = g.inverse_transform(transformed)
    >>> assert np.allclose(x, inversed)

    # Plot various figures
    >>> g.display_mother_function()
    >>> g.plot_data_and_bins(x, bin_frontiers=g.dbf, title='Data and bins before transform.')
    >>> g.plot_hist(x, g.nbins, title='Histogram before transform.')
    >>> g.plot_data_and_bins(x, bin_frontiers=g.rbf, title='Bins after transform.')
    >>> g.plot_data_and_bins(transformed, bin_frontiers=g.dbf, title='Data after transform.')
    >>> g.plot_hist(transformed, g.nbins, title='Histogram after transform.')
    >>> g.display_transform_function()
    >>> g.plot_data_and_bins(inversed, bin_frontiers=g.dbf, title='Data after inverse transform.')

    TODO
    --------
    1. Rewrite to map arbitrary learned distribution to arbitrary distribution
    defined with mother function.
    2. Rewrite so the input data distribution can be also not learned but specified
    using function.
    3. Include cases where the mother function's
    integral is not close to 1 on <0,1>.
    4. Include cases where some bins are desired to be 0.
    5. Include computation of error (distance of current distribution set by nbins
    from the ideal distribution set by the mother function)
    6. Implement sanity checks for data with few equal data points that just
    happen to sit on the bin frontier.
    """

    def __init__(self, validate_input=True):
        # Gaussian mother function with mean = 0.5 and integral from 0 to 1 = 1
        self.mother = norm(loc = 0.5, scale = 0.125).pdf
        self.validate_input=validate_input

        # Will be computed on fit()
        self.fitted = False
        self.N = None
        self.nbins = None
        self.bi = None
        self.dba = None
        self.dbf = None
        self.rbf = None
        self.ratios = None

    def _get_desired_bin_amounts(self):
        """
        Computes the amount of samples that should be in each bin
        in order for the histogram to look gaussian.
        """
        integrals = np.array([integrate.quad(lambda x: self.mother(x), self.dbf[i], self.dbf[i+1])[0]
                     for i in range(len(self.dbf) - 1)])
        dba = self._adjust_sum(integrals * self.N, self.N)
        return dba

    def _adjust_sum(self, a, N):
        """
        Takes an array "a" containg bin amounts that have gaussian
        distribution and adjusts it, so the sum(a) is equal to self.N.
        The resuls will be as gaussian as possible while keeping
        the desired self.N. Alorithm does not adjust bins of amount 1 to 0.

        The result will be more close to real gaussian distribution
        with bigger self.N and a.size (self.nbins).
        Returns only array that has all elements > 0.

        Parameters
        --------
        a : array of ints
            Array of gaussian bin amounts e.g.: [1 2 5 2 1]

        Example
        --------
        a =      [1 2 5 2 1], sum(a)    = 11
        result = [1 1 4 2 1], for self.N = 9
        result = [1 2 4 2 1], for self.N = 10
        result = [1 2 5 2 1], for self.N = 11 (same)
        result = [1 3 5 2 1], for self.N = 12
        result = [1 3 5 3 1], for self.N = 13
        """

        ra = np.ceil(a)
        e = int(N - np.sum(ra))

        while e != 0:
            diff = a - ra
            # Remove bins with amount == 1 from adjust candidates
            # because it could cause 0 bins which we do not want
            ones = np.where(ra == 1)
            adjust_candidates = diff.argsort()
            mask = np.logical_not(np.isin(adjust_candidates, ones))
            adjust_candidates = adjust_candidates[mask]

            # Adjust the adjust candidates by subtracting or adding 1
            adjidx = adjust_candidates[:-e] if e < 0 else adjust_candidates[-e:]
            ra[adjidx] += np.sign(e)

            # Check if there is still difference between sum(re) and N
            e -= adjidx.size * np.sign(e)

        assert e == 0, 'Bug in adjusting number of amounts.'
        assert all(ra > 0), 'There are bins with <1 no. of elements {}, choose smaller nbins.'.format(ra)
        return ra

    def _get_real_bin_frontiers(self, data):
        """
        Computes the x_axis values which would separate the data
        into bins that would yield gaussian histogram.

        Parameters
        --------
        data : array of floats from interval <0.,1.>
            Specifying the position of data points on x axis.
        """
        self.dba = self._get_desired_bin_amounts()
        cs = np.cumsum(np.array(self.dba))[:-1].astype(int)
        rbf = [np.mean([data[x-1], data[x]]) for x in cs]
        rbf = np.concatenate(([0.], rbf, [1.]))
        return rbf

    def _estimate_nbins(self, N, maxnbins=0):
        """
        Estimates number of bins based on self.N.

        Parameters
        --------
        N : int
            Number of samples in the data.

        maxnbins : int, optional, default 333
            Maximum amount of bins.
            If 0, it is set automatically to 333.

        Returns
        -------
        2 < odd int < maxnbins
        """

        if maxnbins == 0: maxnbins = 333  # Automatic limit maxnbins is 333
        if maxnbins % 2 == 0: maxnbins -= 1  # Ensure odd number of maxnbins

        estimate = int(N // 6)
        if estimate % 2 == 0: estimate -= 1  # Ensure odd number of bins
        if estimate < 3: estimate = 3  # Ensure at least 3 bins

        return int(np.min([estimate, maxnbins]))

    def _validate_input(self, data):
        """
        Validation of the input data used to validate inputs to fit() and transform().
        Can be turned off globally by setting self.validate_input = False.
        """
        assert data.ndim == 1, 'Data must be stored in 1-D numpy array. You can use np.ravel(data).'
        assert np.max(data) <= 1. and np.min(data) >= 0., 'Data must be from interval <0,1>'
        assert all(np.isfinite(data)), 'Data must not contain np.nan or np.inf.'

    def fit(self, data, nbins=0, maxnbins=0, sorted=False):
        """
        Takes array of data points, computes and saves attributes necessary for transform().

        Parameters
        --------
        data: array of floats from interval <0.,1.>
            Data to fit on. Number of data points must be at least 10.

        nbins : int, optional, default 0
            Number of bins used to compute the histogram of the data that
            we are trying to gaussify.
            If 0, the number of bins is set automatically.
            If int, it must be > 2 and odd.

        maxnbins : int, optional, default 0
            Maximum amount of bins computed when nbins=0.
            When set to 0, maximum amount of bins = 333.
            More bins means longer computation.

        sorted : boolean, optional, default False
            Set True if the data array is already sorted.
            This will significantly improve the performance
            with large number of data points.
        """

        if self.validate_input: self._validate_input(data)
        self.N = data.size
        assert self.N >= 7, 'Minimum number of samples to fit() on is 10.'
        if not sorted: data = np.sort(data)

        self.nbins = self._estimate_nbins(self.N, maxnbins) if nbins == 0 else nbins
        assert self.nbins > 2 and self.nbins % 2 != 0, 'Choose odd number of bins > 2, current: {}.'.format(self.nbins)

        self.bi = 1 / self.nbins
        self.dbf = [x * self.bi for x in range(self.nbins + 1)]
        self.rbf = self._get_real_bin_frontiers(data)
        self.ratios = self.bi / np.ediff1d(self.rbf)
        self.fitted = True
        return self

    def transform(self, x, inverse=False):
        """
        Applies fitted transformation or inverse transform to the data x.

        Parameters
        --------
        x : array of floats
            Data to transform. Must be from interval <0,1>.

        inverse : boolean, optional, default False
            If True, performes inverse transform.
        """

        if self.validate_input: self._validate_input(x)
        assert self.fitted, 'First, the Gaussifier must be fitted using fit().'

        if inverse:
            cbf=self.dbf # Current bin frontiers of the data x
            tbf=self.rbf # Target bin frontiers of the data x
            operation=operator.truediv
        else:
            cbf=self.rbf
            tbf=self.dbf
            operation=operator.mul

        r = np.zeros_like(x)
        def mold(indices, i):
            r[indices] = tbf[i] + operation((x[indices] - cbf[i]), self.ratios[i])
        [mold(np.where((c1<x) & (x<=c2)), i) for i, (c1, c2) in enumerate(zip(cbf, cbf[1:]))]

        return r

    def inverse_transform(self, x):
        """
        Applies fitted inverse transform to the data x.

        Parameters
        --------
        x : array of floats
            Data to inverse transform. Must be from interval <0,1>.
        """
        return self.transform(x, inverse=True)

    def display_mother_function(self, figsize=(15,2)):
        """
        Displays matplotlib plot of the self.mother.

        Parameters
        --------
        figsize : tuple (width, height), optional, default (15,2)
            Desired size of the figure.
        """
        assert self.fitted, 'First, the Gaussifier must be fitted using fit().'

        x_axis = np.arange(0, 1.01, 0.01)

        import matplotlib.pyplot as plt
        plt.figure(figsize=figsize)
        plt.plot(x_axis, self.mother(x_axis))
        plt.title('Gaussian mother function with {} bins'.format(self.nbins))
        for xc in self.dbf:
            plt.axvline(x=xc, color='r', alpha=0.25)
        plt.xlim(0, 1)
        plt.show()
        plt.close()

    def display_transform_function(self, figsize=(15,2)):
        """
        Displays matplotlib plot of the fitted transform function.

        Parameters
        --------
        figsize : tuple (width, height), optional, default (15,2)
            Desired size of the figure.
        """
        assert self.fitted, 'First, the Gaussifier must be fitted using fit().'

        x_axis = np.arange(0, 1.01, 0.01)
        transformed = self.transform(x_axis)

        import matplotlib.pyplot as plt
        plt.figure(figsize=figsize)
        plt.plot(x_axis, transformed)
        plt.title('Learned monotonic piecewise linear transform function')
        for xc in self.rbf:
            plt.axvline(x=xc, color='r', alpha=0.25)
        plt.xlim(0, 1)
        plt.show()
        plt.close()


    def plot_data_and_bins(self, data, bin_frontiers, title=None, figsize=(15,2)):
        """
        Displays matplotlib plot of specified data and bin frontiers.

        Parameters
        --------
        data : array of floats from interval <0.,1.>
            Specifying the position of data points on x axis.

        bin_frontiers : array of floats from interval <0.,1.>
            Specifying the position of bin fronteirs on x axis.

        figsize : tuple (width, height), optional, default (15,2)
            Desired size of the figure.
        """
        import matplotlib.pyplot as plt
        plt.figure(figsize=figsize)
        plt.plot(data, np.zeros_like(data) + 0.1, 'x')
        plt.title(title)
        for xc in bin_frontiers:
            plt.axvline(x=xc, color='r', alpha=0.25)
        plt.xlim(0, 1)
        plt.show()
        plt.close()

    def plot_hist(self, data, nbins=None, title=None, figsize=(15,2)):
        """
        Displays matplotlib histogram of the specified data and nbins.

        Parameters
        --------
        data : array of floats from interval <0.,1.>
            Specifying the position of data points on x axis.

        nbins : int, optional, default None
            Specifying the number of bins of the histogram.
            If None, the number will be automatically set by matplotlib.

        figsize : tuple (width, height), optional, default (15,2)
            Desired size of the figure.
        """
        import matplotlib.pyplot as plt
        plt.figure(figsize=figsize)
        plt.hist(data, nbins, range=(0., 1.))
        plt.title(title)
        plt.xlim(0, 1)
        plt.show()
        plt.close()
